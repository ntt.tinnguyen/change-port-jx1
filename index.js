var fs = require('fs');
var CronJob = require('cron').CronJob;
var os = require('os');
const axios = require('axios');

var CRON_EXPRESSION = '0-59/5 * * * *';
var UPDATE_CONFIG_ENDPOINT = 'http://volamvoz.com/autoupdate/config.ini';

var job = new CronJob(CRON_EXPRESSION, function() {
  console.log('ChangePort job is running. Current time:', new Date().toLocaleString('en-VN'))
  changePort();
}, null, true, 'Asia/Ho_Chi_Minh');

console.log('App is started.');
console.log('ChangePort cron job runs every 5 minute.')
job.start();

async function changePort() {
  console.log('START');
  try {
    // Get autoupdate config
    let updateConfig = await axios.get(UPDATE_CONFIG_ENDPOINT);
    let updateConfigObject = exportConfigObject(updateConfig.data, os.EOL);

    // Get current config
    let currentConfig = await fs.promises.readFile('config.ini', 'utf-8',);
    let currentConfigObject = exportConfigObject(currentConfig, os.EOL);

    let currentport = parseInt(currentConfigObject.GameServPort);
    let updatePort = parseInt(updateConfigObject.GameServPort);
    console.log(`currentport=${currentport}. updatePort=${updatePort}`)

    // If current port != autoupdate port => replace
    if (currentport !== updatePort) {
      console.log(` Port is changed. From ${currentport} to ${updatePort}`)
      var newValue = currentConfig.replace(
        currentport.toString(), 
        updatePort.toString());
      // save config
      fs.writeFile('config.ini', newValue, 'utf-8', function (err) {
        if (err) throw err;
        console.log('ChangePort completed.');
      });
    } else {
      console.log(` Port is not changed. Update nothing. currentport=${currentport}.`)
    }
  } catch (error) {
    console.error('Change port error', error)
  }
  console.log('END');
}

function exportConfigObject(data, delimiter) {
  var configObject = {};
      var splitProperty;
  data.split(delimiter).forEach(line => {
    splitProperty = line.split('=');
    if (splitProperty.length === 2) {
        configObject[splitProperty[0]] = splitProperty[1];
    }
  })
  return configObject;
}
